# Temperature Conversion Lambda Function Documentation
> Xingyu, Zhang (NetID: xz365)

## Overview

This [AWS Lambda function](./src/main.rs) provides an API endpoint for converting temperatures between Celsius and Fahrenheit. It is designed to receive a temperature value and the target scale (Celsius or Fahrenheit) as input, perform the conversion, and return the original and converted temperature values along with the target scale.

## Deployment

This function is deployed as a POST API method, making it accessible via HTTP requests. It is typically integrated with AWS services like Amazon API Gateway, which routes incoming HTTP requests to the Lambda function.


## Environment and Dependencies
The function is written in Rust, utilizing the `lambda_runtime`, `serde`, `serde_json`, and `tokio` crates.
It is deployed with AWS Lambda's default execution role, with permissions adjusted as necessary for integration with other AWS services.

## Input

To use this function, send a JSON object in the body of a POST request with the following structure:

```json
{
  "temperature": <double>, // The temperature value to convert
  "target_scale": "<string>" // The target temperature scale, either "Celsius" or "Fahrenheit"
}
```
#### [Example Request Body](./event.json)
```json
{
  "temperature": 68,
  "target_scale": "Celsius"
}
```

## Processing
Upon receiving a valid request, the function:

1. Validates the input parameters.
2. Converts the temperature from its current scale to the target scale.
3. Constructs a response object with the original and converted temperature values, along with the target scale.

## Output
The function responds with a JSON object structured as follows:
```json
{
  "statusCode": 200,
  "headers": {
    "Content-Type": "application/json"
  },
  "body": {
    "original_temperature": <double>,
    "converted_temperature": <double>,
    "target_scale": "<string>"
  }
}
```
#### [Example Response Body](event.json)

```json
{
    "body":" {
      \"original_temperature\":68.0,
      \"converted_temperature\":20.0,
      \"target_scale\":\"Celsius\"
    }",
    "headers":{
      "Content-Type":"application/json"
    },
    "statusCode":200
}
```

## Error Handling
In case of an error, such as an invalid `target_scale`, the function returns a JSON object with a `statusCode` of 400 and an error message:
```json
{
  "statusCode": 400,
  "headers": {
    "Content-Type": "application/json"
  },
  "body": {
    "error": "Invalid target scale"
  }
}
```
The error massages also includes following when the input JSON missing certain field(s).
```json
# temperature missed
{
    "errorType": "&alloc::boxed::Box<dyn core::error::Error + core::marker::Send + core::marker::Sync>",
    "errorMessage": "missing field `temperature`"
}
# target_scale missed
{
    "errorType": "&alloc::boxed::Box<dyn core::error::Error + core::marker::Send + core::marker::Sync>",
    "errorMessage": "missing field `temperature`"
}
```

## Logging
The function logs the received event JSON, any deserialization errors, and the parsed request for debugging purposes.

## Using the API from the Terminal

This section provides examples of how to invoke the Temperature Conversion API from different terminal environments. The __invoke URL (API Endpoint)__ is: https://t6j87fnkpi.execute-api.us-east-1.amazonaws.com/default/temperature_converter.

Adjust the `temperature` and `target_scale` values as needed.
### Unix-based Terminals (Linux/macOS)

You can use `curl` to send a request to the API from a Unix-based terminal.  Here's an example command:

```bash
curl -X POST "https://t6j87fnkpi.execute-api.us-east-1.amazonaws.com/default/temperature_converter" \
-H "Content-Type: application/json" \
-d '{"temperature": 68, "target_scale": "Celsius"}'
```
### Windows Command Prompt
In Windows Command Prompt, you can also use curl to send a request to the API. The syntax is slightly different due to how quotes are handled. Here's an example command:
```cmd
curl -X POST "https://t6j87fnkpi.execute-api.us-east-1.amazonaws.com/default/temperature_converter" -H "Content-Type: application/json" -d "{\"temperature\": 68, \"target_scale\": \"Celsius\"}"
```

### PowerShell
For users on Windows using PowerShell, the command differs slightly due to its handling of JSON strings. Here's how to invoke the API from PowerShell:
```powershell
Invoke-RestMethod -Method Post -Uri <API_ENDPOINT> -ContentType "application/json" -Body (@{temperature=68; target_scale="Celsius"} | ConvertTo-Json)
```

## APPENDIX
![lambda function test](./AWS_LAMBDA_TEST.png)
![api test](./API_POST_TEST.png)
![api deployment](./API_DEPLOY_INFO.png)
