use lambda_runtime::{Error, handler_fn, Context};
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};
use std::collections::HashMap;

#[derive(Deserialize, Debug)] // Ensure Debug is derived for logging
struct Request {
    temperature: f64,
    target_scale: String,
}

#[derive(Serialize)]
struct Response {
    original_temperature: f64,
    converted_temperature: f64,
    target_scale: String,
}

#[derive(Serialize)]
struct LambdaResponse {
    statusCode: u16,
    headers: HashMap<String, String>,
    body: String,
}

async fn convert_temperature(event: Value, _: Context) -> Result<Value, Error> {
    // Log the raw event JSON
    println!("Received event JSON: {:?}", event);

    // Attempt to deserialize the event to your Request struct
    let request: Request = serde_json::from_value(event.clone()) // Clone event for logging, if necessary
        .map_err(|err| {
            // Log deserialization errors
            println!("Error deserializing event to Request: {:?}", err);
            err
        })?;

    // Log the deserialized request
    println!("Parsed request: {:?}", request);

    let converted_response = match request.target_scale.to_lowercase().as_str() {
        "celsius" => Response {
            original_temperature: request.temperature,
            converted_temperature: (request.temperature - 32.0) * 5.0 / 9.0,
            target_scale: "Celsius".to_string(),
        },
        "fahrenheit" => Response {
            original_temperature: request.temperature,
            converted_temperature: (request.temperature * 9.0 / 5.0) + 32.0,
            target_scale: "Fahrenheit".to_string(),
        },
        _ => {
            return Ok(json!({
                "statusCode": 400,
                "headers": { "Content-Type": "application/json" },
                "body": json!({ "error": "Invalid target scale" }).to_string(),
            }));
        }
    };

    Ok(json!({
        "statusCode": 200,
        "headers": { "Content-Type": "application/json" },
        "body": serde_json::to_string(&converted_response)?,
    }))
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    lambda_runtime::run(handler_fn(convert_temperature)).await?;
    Ok(())
}
